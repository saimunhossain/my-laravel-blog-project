@extends('main')

@section('title', '| Homepage')

@section('content')
        <div class="row">
            <div class="col-md-12">
                <div class="jumbotron">
                      <h1>Welcome to My blog!</h1>
                      <p class="lead">This is my first blog. I want write about programming related blogs. Hope you guys enjoy it. If you want to get more blogs about programming, you will have have to share my blogs to social sites as much as you can. And thanks for visiting my website.</p>
                      <p><a class="btn btn-primary btn-lg" href="#" role="button">Popular Post</a></p>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-8">

              @foreach($posts as $post)

                    <div class="row">
                        <h2>{{ $post->title }}</h2>
                        <p>{{ substr(strip_tags($post->body), 0, 300) }}{{ strlen(strip_tags($post->body)) > 300 ? "..." : "" }}</p>
                        <a href="{{ url('blog/'.$post->slug) }}" class="btn btn-primary">Read More</a>
                    </div>

                <hr>
               @endforeach 
            </div>

            <div class="col-md-3 col-md-offset-1">
                <h2>Sidebar</h2>
            </div>
        </div>
@endsection       